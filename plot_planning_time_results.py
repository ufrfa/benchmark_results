import matplotlib.pyplot as plt
import pandas
import numpy as np
import os

colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']

def getProblemFromFilename(filename):
    name_parts = filename.split('_')
    for i in range(0, len(name_parts) - 1):
        if str(name_parts[i]) == "move" and str(name_parts[i+1]) == "to":
            return str(name_parts[i+2]) + '_' + str(name_parts[i+3])
    


data_folder = "results_planning_time_5_to_200/" #"results_pose_target/" #results_prm_no_roadmap/
data_file_name = "2023-05-31_22-01-32_move_to_auspresseinheit_D_pose_target_results" #without .csv
plot_folder = "plots/"

#Planner,Run,Success,Length_In_Joint_Space,Length_In_Cartesian_Space(Endeffector),Smoothness,Time,Weighted_Length,Shoulder_Pan_Joint,Shoulder_Lift_Joint,Elbow_Joint,Wrist_1_Joint,Wrist_2_Joint,Wrist_3_Joint

df = pandas.DataFrame()
for (dirpath, dirnames, filenames) in os.walk(data_folder):
    for filename in filenames:
        if filename.endswith('.csv'):
            data = pandas.read_csv(data_folder + filename)
        name_parts = filename.split('_')
        max_planning_time = 0
        problem_name = 'magneteinheit_A'
        for i in range(0, len(name_parts)-2):
            if str(name_parts[i]).isnumeric() and str(name_parts[i+1]) == 'seconds':
                max_planning_time = int(name_parts[i])
                problem_name = getProblemFromFilename(filename)
                break
        data['Problem'] = str(problem_name)
        data['Max_Planning_Time'] = max_planning_time
        df = pandas.concat([df, data], axis=0)

print(df)

#for index, row in df[df['Planner'] == 'TRRT'].iterrows():
#    print(row['Problem'] + ' ' + str(row['Max_Planning_Time']) + ' seconds: ' + str(row['Length_In_Joint_Space']))
        


planners = df['Planner'].unique()
problems = df['Problem'].unique()
planning_times = df['Max_Planning_Time'].unique()


print(problems)
print(planning_times)
print(planners)


for problem in problems:
    problem_specific_data = df[df['Problem'] == str(problem)]
    data = pandas.DataFrame(index=planning_times)
    data['Problem'] = problem
    for planner in planners:
        data[str(planner) + '_max'] = 0.0
        data[str(planner) + '_min'] = 0.0
        #data[str(planner) + '_intervall'] = 0.0
        #data[str(planner) + '_median'] = 0.0
        data[str(planner) + '_mean'] = 0.0
    #print(data)
    for time in planning_times:
        time_problem_specific_data = problem_specific_data[problem_specific_data['Max_Planning_Time'] == time]
        for planner in planners:
            #print(planner)
            #print(time)
            #print(problem)
            planner_time_problem_specific_data = time_problem_specific_data[time_problem_specific_data['Planner'] == str(planner)]
            planner_time_problem_specific_data = planner_time_problem_specific_data.loc[planner_time_problem_specific_data['Success'] == True]
            #print(planner_time_problem_specific_data)
            #print(planner_time_problem_specific_data['Length_In_Joint_Space'].max())
            data.at[time, str(planner) + '_max'] = planner_time_problem_specific_data['Length_In_Joint_Space'].max()
            data.at[time, str(planner) + '_min'] = planner_time_problem_specific_data['Length_In_Joint_Space'].min()
            #data.at[time, str(planner) + '_intervall'] = planner_time_problem_specific_data['Length_In_Joint_Space'].max() - planner_time_problem_specific_data['Length_In_Joint_Space'].min()
            #data.at[time, str(planner) + '_median'] = planner_time_problem_specific_data['Length_In_Joint_Space'].median()
            data.at[time, str(planner) + '_mean'] = planner_time_problem_specific_data['Length_In_Joint_Space'].mean()
            #print(data)
    print(data)
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)
    data = data.sort_index(ascending=True)
    for i in range(0, len(planners)):
        #print(data[str(planners[i]) + '_median'])
        #ax.plot(data.index.values.tolist(), data[str(planners[i]) + '_median'], '-', label=str(planners[i]), color=str(colors[i]))
        ax.plot(data.index.values.tolist(), data[str(planners[i]) + '_mean'], '-', label=str(planners[i]), color=str(colors[i]))
        ax.fill_between(data.index.values.tolist(), data[str(planners[i]) + '_max'], data[str(planner) + '_min'], alpha=0.2, color=str(colors[i]))
        #ax.errorbar(planning_times, data[str(planner) + '_median'], yerr=data[str(planner) + '_intervall'], label=str(planner), errorevery=(0, 14))
    ax.legend()
    fig.suptitle(problem)
    plt.show()
        
    
    



        



'''
boxplot('Length_In_Joint_Space', planners_data, planners)
boxplot('Time', planners_data, planners)
boxplot('Length_In_Cartesian_Space(Endeffector)', planners_data, planners)
boxplot('Weighted_Length', planners_data, planners)
boxplot('Shoulder_Pan_Joint', planners_data, planners)
boxplot('Shoulder_Lift_Joint', planners_data, planners)
boxplot('Elbow_Joint', planners_data, planners)
boxplot('Wrist_1_Joint', planners_data, planners)
boxplot('Wrist_2_Joint', planners_data, planners)
boxplot('Wrist_3_Joint', planners_data, planners)
joint_distances_bar_graph(planners_data, planners)
successfull_runs_bar_graph(planners_data, planners)

'''
