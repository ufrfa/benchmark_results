import os
import pandas
import time


benchmark_results = []

path  = "results_processing_playground/"
for (dirpath, dirnames, filenames) in os.walk(path):
    for filename in filenames:
        if str(filename).endswith('.csv') and not 'combined' in str(filename):
            df = pandas.read_csv(dirpath + '/' + filename)
            #print(filename)
            benchmark_results.append(df)

planners = benchmark_results[0]['Planner'].unique()

planners_scoresheet = pandas.DataFrame(index=planners)
#planners_scoresheet['Planner'] = planners
planners_scoresheet['Score_speed'] = 0
planners_scoresheet['Score_quality'] = 0

def calculate_score(planners, best_value, worst_value, planners_ranking, ascending, parameter, score_mode, weight=1):
    global planners_scoresheet
    planners_ranking = planners_ranking.sort_values(by=[str(parameter)], ascending=ascending)
    planners_ranking = planners_ranking.reset_index(drop=True)
    for index, row in planners_ranking.iterrows():
        #planners_scoresheet.at[str(row['Planners']), 'Score'] += weight*(len(planners) - (len(planners))*abs((best_value - row[str(parameter)]))/abs(best_value - worst_value))
        planners_scoresheet.at[str(row['Planners']), score_mode] += weight*(len(planners) - (len(planners))*abs((best_value - row[str(parameter)]))/abs(best_value + worst_value))
        #planners_scoresheet = planners_scoresheet.sort_values(by=[score_mode], ascending=False)

def calculate_spread(planner_data, parameter):
    q1 = planner_data[str(parameter)].quantile(0.25)
    q3 = planner_data[str(parameter)].quantile(0.75)
    iqr = q3 - q1
    sorted = planner_data.sort_values(by=[str(parameter)], ascending=True)
    sorted = sorted.reset_index(drop=True)
    spread = 0
    for index, row in sorted.iterrows():
        if row[str(parameter)] >= (q1 - 1.5*iqr):
            spread = row[str(parameter)]
            break
    sorted = planner_data.sort_values(by=[str(parameter)], ascending=False)
    sorted = sorted.reset_index(drop=True)
    for index, row in sorted.iterrows():
        if row[str(parameter)] <= (q3 + 1.5*iqr):
            spread = abs(row[str(parameter)] - spread)
            break
    return spread


for result in benchmark_results:
    planners_data = []
    for planner in planners:
        planners_data.append(result[result['Planner'] == str(planner)])
    num_successfull_runs = []
    mean_time = []
    time_spread = []
    mean_joint_space_distance = []
    joint_space_distance_spread = []
    mean_weighted_distance = []
    weighted_distance_spread = []
    for planner_data in planners_data:
        num_successfull_runs.append(len(planner_data["Success"].loc[planner_data['Success'] == True]))
        mean_time.append(planner_data['Time'].mean())
        time_spread.append(calculate_spread(planner_data, 'Time'))
        mean_joint_space_distance.append(planner_data['Length_In_Joint_Space'].mean())
        joint_space_distance_spread.append(calculate_spread(planner_data, 'Length_In_Joint_Space'))
        mean_weighted_distance.append(planner_data['Weighted_Length'].mean())
        weighted_distance_spread.append(calculate_spread(planner_data, 'Weighted_Length'))
    #print(mean_time)
    planners_ranking = pandas.DataFrame()
    planners_ranking['Planners'] = planners
    planners_ranking['Successfull_Runs'] = num_successfull_runs
    planners_ranking['Mean_Time'] = mean_time
    planners_ranking['Time_Spread'] = time_spread
    planners_ranking['Mean_Joint_Space_Distance'] = mean_joint_space_distance
    planners_ranking['Joint_Space_Distance_Spread'] = joint_space_distance_spread
    planners_ranking['Mean_Weighted_Distance'] = mean_weighted_distance
    planners_ranking['Weighted_Distance_Spread'] = weighted_distance_spread
    #print(planners_ranking)
    best_value = planners_ranking['Successfull_Runs'].max()
    worst_value = 0
    calculate_score(planners, best_value, worst_value, planners_ranking, False, 'Successfull_Runs', 'Score_speed')
    calculate_score(planners, best_value, worst_value, planners_ranking, False, 'Successfull_Runs', 'Score_quality')
    best_value = planners_ranking['Mean_Time'].min()
    worst_value = planners_ranking['Mean_Time'].max()
    calculate_score(planners, best_value, worst_value, planners_ranking, True, 'Mean_Time', 'Score_speed')
    best_value = planners_ranking['Time_Spread'].min()
    worst_value = planners_ranking['Time_Spread'].max()
    calculate_score(planners, best_value, worst_value, planners_ranking, True, 'Time_Spread', 'Score_speed', weight=0.5)
    best_value = planners_ranking['Mean_Joint_Space_Distance'].min()
    worst_value = planners_ranking['Mean_Joint_Space_Distance'].max()
    calculate_score(planners, best_value, worst_value, planners_ranking, True, 'Mean_Joint_Space_Distance', 'Score_quality')
    best_value = planners_ranking['Joint_Space_Distance_Spread'].min()
    worst_value = planners_ranking['Joint_Space_Distance_Spread'].max()
    calculate_score(planners, best_value, worst_value, planners_ranking, True, 'Joint_Space_Distance_Spread', 'Score_quality')
    best_value = planners_ranking['Mean_Weighted_Distance'].min()
    worst_value = planners_ranking['Mean_Weighted_Distance'].max()
    calculate_score(planners, best_value, worst_value, planners_ranking, True, 'Mean_Weighted_Distance', 'Score_quality', weight=0.5)
    best_value = planners_ranking['Weighted_Distance_Spread'].min()
    worst_value = planners_ranking['Weighted_Distance_Spread'].max()
    calculate_score(planners, best_value, worst_value, planners_ranking, True, 'Weighted_Distance_Spread', 'Score_quality', weight=0.5)
#planners_scoresheet = planners_scoresheet.sort_values(by=['Score_speed'], ascending=False)
#print(planners_scoresheet)
planners_scoresheet = planners_scoresheet.sort_values(by=['Score_quality'], ascending=False)
print(planners_scoresheet)
    #planners_scoresheet = planners_scoresheet.reset_index(drop=True)
    
    





