import csv
import os
import pandas


path = "results_projection_planners_150_runs_joints_1_2_3/"

df = pandas.DataFrame()
for (dirpath, dirnames, filenames) in os.walk(path):
    for filename in filenames:
        if filename.endswith('.csv'):
            data = pandas.read_csv(path + filename)
            df = pandas.concat([df, data], axis=0)
df.to_csv(path + 'combined.csv', index=False)
