import os
import json
from enum import Enum


class FilterAttribute(Enum):
    TRAJ_LEN = 'traj_len'
    CART_TRAJ_LEN = 'cart_traj_len'
    TIME = 'time'
    WEIGHTED_DISTANCE = 'weighted_distance'
    SMOOTHNESS = 'smoothness'
    DISTANCE_SHOULDER_PAN_JOINT = 'distance_shoulder_pan_joint'
    DISTANCE_SHOULDER_LIFT_JOINT = 'distance_shoulder_lift_joint'
    DISTANCE_ELBOW_JOINT = 'distance_elbow_joint'
    DISTANCE_WRIST_1_JOINT = 'distance_wrist_1_joint'
    DISTANCE_WRIST_2_JOINT = 'distance_wrist_2_joint'
    DISTANCE_WRIST_3_JOINT = 'distance_wrist_3_joint'


class Filter():
    def __init__(self, planner="", goal_name="", attribute=FilterAttribute.TRAJ_LEN):
        self.__planner = planner
        self.__goal_name = goal_name
        self.__attribute = attribute
        self.__ordered_trajectories = {}

    def getOrderedTrajectories(self):
        return self.__ordered_trajectories

    def __insert_data(self, trajectory_data):
        self.__ordered_trajectories[trajectory_data['id']] = trajectory_data['metrics'][self.__attribute.value]
        self.__ordered_trajectories = dict(sorted(self.__ordered_trajectories.items(), key=lambda item: item[1]))

    def filter_data(self, trajectory_data):
        if not self.__planner == "" and trajectory_data['planner'] == self.__planner:
            if not self.__goal_name == "" and trajectory_data['goal']['name'] == self.__goal_name:
                self.__insert_data(trajectory_data)
            elif self.__goal_name == "":
                self.__insert_data(trajectory_data)
        elif self.__planner == "":
            if not self.__goal_name == "" and trajectory_data['goal']['name'] == self.__goal_name:
                self.__insert_data(trajectory_data)
            elif self.__goal_name == "":
                self.__insert_data(trajectory_data)


filter = Filter(planner="", goal_name="move_to_magneteinheit_A", attribute=FilterAttribute.TRAJ_LEN)

path  = "trajectories/results_projection_planners_150_runs_joints_1_2/"
for (dirpath, dirnames, filenames) in os.walk(path):
    for filename in filenames:
        file = open(path + filename)
        #print(path + filename)
        trajectory_data = json.load(file)
        file.close()
        filter.filter_data(trajectory_data)

for key, value in filter.getOrderedTrajectories().items():
    print(str(key) + ":         " + str(value))
