# Benchmark Results

In this repository, results from benchmarks as well as plots, created trajectories and tools to process results can be found.

# Projection Benchmarks

In the folders:
- results_projection_planners_150_runs_joints_1_2 
- results_projection_planners_150_runs_joints_1_2_3

Projection based planners were benchmarked with different projections. The outcome is described in comparison_projections.md

# All Planners Benchmarks

In the folder:
-  results_all_planners_150_runs_2

All planners were benchmarked. This is the main benchmarking experiment. 
