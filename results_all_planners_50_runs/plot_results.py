import matplotlib.pyplot as plt
import pandas
import numpy as np

colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']

def boxplot(attribute, planners_data, planners):
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)
    ax.set_title(str(attribute))
    #ax.set_xlabel('Planners')
    ax.set_ylabel(str(attribute))
    relevant_planners = []
    relevant_planners_data = []
    if attribute == 'Time':
        for i in range(0, len(planners)):
            planner = planners[i]
            if not 'star' in str(planner) and not 'LBTRRT' in str(planner) and not 'SPARS' in str(planner):
                relevant_planners.append(str(planner))
                relevant_planners_data.append(planners_data[i])
    else:
        relevant_planners = planners
        relevant_planners_data = planners_data
    data = []
    for planner_data in relevant_planners_data:
        data.append(planner_data[planner_data[str(attribute)] > 0][str(attribute)])
    ax.boxplot(data, labels=relevant_planners, sym='')
    ax.tick_params(axis='x', rotation=90)
    plot_file_name = "plots/" + data_file_name + "_" + str(attribute) + "_boxplot.png"
    plt.savefig(plot_file_name)
    plt.show()

def successfull_runs_bar_graph(planners_data, planners):
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)
    runs = len(planners_data[0]['Run'])
    data = []
    for planner_data in planners_data:
        data.append(len(planner_data["Success"].loc[planner_data['Success'] == True]))
    ax.bar(planners, data, color=colors)
    ax.set_title('Successfull Runs')
    #ax.xlabel('Planners')
    ax.set_ylabel('Num Successfull Runs (max: ' + str(runs) + ')')
    ax.tick_params(axis='x', rotation=90)
    plot_file_name = "plots/" + data_file_name + "_successfull_runs.png"
    plt.savefig(plot_file_name)
    plt.show()

def joint_distances_bar_graph(planners_data, planners):
    joints = ['Shoulder_Pan_Joint', 'Shoulder_Lift_Joint', 'Elbow_Joint', 'Wrist_1_Joint', 'Wrist_2_Joint', 'Wrist_3_Joint']
    data_dict = {}
    normalized_data_dict = {}
    for joint in joints:
        values = []
        for data in planners_data:
            values.append(data[data['Success'] == True][str(joint)].mean())
        data_dict[str(joint)] = values
    path_distances = []
    for i in range(0, len(planners)):
        path_distance = 0
        for joint in joints:
            path_distance += data_dict[str(joint)][i]
        path_distances.append(path_distance)
    for joint in joints:
        values = []
        for i in range(0, len(path_distances)):
            values.append(data_dict[str(joint)][i] / path_distances[i])
        normalized_data_dict[str(joint)] = values
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)
    bottom = np.zeros(len(planners))
    for joint_names, joint_values in data_dict.items():
        p = ax.bar(planners, joint_values, 0.5, label=joint_names, bottom=bottom)
        bottom += joint_values
    ax.set_title('Mean Distance covered by each joint')
    ax.set_ylabel('Rad')
    ax.tick_params(axis='x', rotation=90)
    ax.legend(loc="upper right")
    plot_file_name = "plots/" + data_file_name + "_" + "distance_by_joint.png"
    plt.savefig(plot_file_name)
    plt.show()
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)
    bottom = np.zeros(len(planners))
    for joint_names, joint_values in normalized_data_dict.items():
        p = ax.bar(planners, joint_values, 0.5, label=joint_names, bottom=bottom)
        bottom += joint_values
    ax.set_title('Normalized Mean Distance covered by each joint')
    ax.set_ylabel('Rad')
    ax.tick_params(axis='x', rotation=90)
    ax.legend(loc="upper right")
    plot_file_name = "plots/" + data_file_name + "_" + "normalized_distance_by_joint.png"
    plt.savefig(plot_file_name)
    plt.show()
    plt.show()







data_folder = "" #"results_pose_target/" #results_prm_no_roadmap/
data_file_name = "combined" #without .csv

#Planner,Run,Success,Length_In_Joint_Space,Length_In_Cartesian_Space(Endeffector),Smoothness,Time,Weighted_Length,Shoulder_Pan_Joint,Shoulder_Lift_Joint,Elbow_Joint,Wrist_1_Joint,Wrist_2_Joint,Wrist_3_Joint

df = pandas.read_csv(data_folder + data_file_name + ".csv")
planners = df['Planner'].unique()
planners_data = []
for planner in planners:
    planners_data.append(df[df['Planner'] == str(planner)])
boxplot('Length_In_Joint_Space', planners_data, planners)
boxplot('Time', planners_data, planners)
boxplot('Length_In_Cartesian_Space(Endeffector)', planners_data, planners)
boxplot('Weighted_Length', planners_data, planners)
boxplot('Shoulder_Pan_Joint', planners_data, planners)
boxplot('Shoulder_Lift_Joint', planners_data, planners)
boxplot('Elbow_Joint', planners_data, planners)
boxplot('Wrist_1_Joint', planners_data, planners)
boxplot('Wrist_2_Joint', planners_data, planners)
boxplot('Wrist_3_Joint', planners_data, planners)
joint_distances_bar_graph(planners_data, planners)
successfull_runs_bar_graph(planners_data, planners)
