# Comparison of different projections for path planning

## Projections
joints_1_2:
- shoulder_pan_joint
- shoulder_lift_joint

joints_1_2_3
- shoulder_pan_joint
- shoulder_lift_joint
- elbow_joint

## Successfull Runs

Bei joints_1_2_3 hat LBKPIECE bei fast allen Problemen nur 120-130 von 150 Pfaden gefunden. Außerdem hatten KPIECE und ProjEst bei move_to_magneteinheit_B ebenfalls nur 130/150.

## Time

Bei joints_1_2_3 brauchen BKPIECE und besonders LBKPIECE deutlich länger (BKPIECE average 1sec, LBKPIECE average 3sec). KPIECE und ProjEST sind ähnlich zu joints_1_2.
Bei joints_1_2 alle Algorithmen fast immer unter 1sec.

Bei move_to_auspresseinheit_C haben KPIECE und ProjEST deutliche Streuung nach oben bei joints_1_2 und bei joints_1_2_3.

Ebenso bei move_to_magneteinheit_B wobei der Effekt hier bei joints_1_2 stärker ist.

Ebenso bei move_to_magneteinheit_C.

--> LBKPIECE und (weniger stark) BKPIECE brauchen i.A. deutlich länger bei joints_1_2. Bei ein paar Problemen haben KPIECE und ProjEST Streuungen nach oben.

## Trajectory Length

Über alle Probleme hinweg sind alle Algorithmen bei joints_1_2 und joints_1_2_3 ähnlich (average ca 3m bei allen). Streuung nach oben ist bei joints_1_2_3 etwas stärker (übersteigt (vor allem bei LBKPIECE) 7m, bei joints_1_2 konstant unter 7m).


Fazit soweit: joints_1_2 ist schneller und sicherer hat aber keine schlechteren Pfade. Weitermachen mit joints_1_2
